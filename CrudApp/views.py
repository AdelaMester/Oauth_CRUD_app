from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
import urllib.parse
from uuid import uuid4
from requests.auth import HTTPBasicAuth
import requests
import json
import re


client_id = "469f777806ca6a7f24e8"
client_secret = "your secret code"
redirect_uri = "http://34.105.148.8:8000/github_callback"



# Create your views here.
def index(request):
    return render(request, "CrudApp/index.html")

def make_authorization_url(request):
    state = str(uuid4())
    params = {"client_id": client_id,
              "response_type": "code",
              "redirect_uri": redirect_uri,
              "state": state,
              "duration": "temporary",
              "scope": "user"
              }
    return HttpResponseRedirect('https://github.com/login/oauth/authorize?' + urllib.parse.urlencode(params))

def github_callback(request):
    if request.method == "GET":
        error = request.GET.get('error', '')
        if error:
            return "error: " + error
        code = request.GET.get('code')
        state= request.GET.get('state')
        access_token = str(get_token(code, state))
        print((access_token))
    return render(request,"CrudApp/callback.html",{
        "message": "Your GitHub username is: "+ (get_username(access_token)),
        "token": access_token
    })


def get_token(code,state):
    post_data = {"grant_type": "authorization_code",
                 "code": code,
                 "redirect_uri": redirect_uri,
                 "client_id": client_id,
                 "client_secret": client_secret,
                 "state":state
                 }
    response = requests.post("https://github.com/login/oauth/access_token", data=post_data)
    print("response.text: "+response.text)
    at = response.text[13:53]
    print(at)
    return (at)


def get_username(access_token):
    #print("at:" +access_token)
    headers = {"Authorization": "token " + str(access_token)}
    response = requests.get("https://api.github.com/user", headers=headers)
    print(response.text)
    me_json = response.json()
    print("username: "+me_json["login"])
    return (me_json["login"])

def email(request):
    if request.method == 'GET':
        return render(request, "CrudApp/email.html")

def delete(request):
    if request.method == 'GET':
        return render(request, "CrudApp/delete.html")

def list_emails(request):
    if request.method == 'GET':
        return render(request, "CrudApp/list_emails.html")

def email_visibility(request):
    if request.method == 'GET':
        return render(request, "CrudApp/email_visibility.html")


