from django.shortcuts import render
from . import views
from django.urls import path

urlpatterns =[
    path("", views.index, name="index"),
    path("make_authorization_url/", views.make_authorization_url, name="make_authorization_url"),
    path("github_callback/", views.github_callback, name="github_callback"),
    path("get_token/", views.get_token, name="get_token"),
    path("get_username/", views.get_username, name="get_username"),
    path("email/", views.email, name="email"),
    path("delete/", views.delete, name="delete"),
    path("list_emails/", views.list_emails, name="list_emails"),
    path("email_visibility/", views.email_visibility, name="email_visibility")
]